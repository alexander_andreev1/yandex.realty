'''
	Test MVP 1.0.0 Area method
	Only three params: District, BuildingType, Square, Price, Time (Days)
'''
import random
from math import *

DISTRICT_list = ["Гольяново", "Метрогородок", "Северное Измайлово"]
DISTRICT_dict = {}

BUILDINGTYPE_list = ["Кирпич", "Монолит", "Панель", "Кирпич-монолит", "Блок"]
BUILDINGTYPE_dict = {}

ALL_ADS = []

def __start__():
	global DISTRICT_list, DISTRICT_dict, BUILDINGTYPE_list, BUILDINGTYPE_dict;

	for key in range(len(DISTRICT_list)):
		DISTRICT_dict[str(DISTRICT_list[key])] = str(key + 1)
	for key in range(len(BUILDINGTYPE_list)):
		BUILDINGTYPE_dict[str(BUILDINGTYPE_list[key])] = str(key + 1)
	print(DISTRICT_dict)

def __moderate_ad__(number_of_moderations):
	for i in range(number_of_moderations):
		if number_of_moderations > 0:
			temp_district = random.randint(1, 3 - 1)
			temp_building_type = random.randint(1, 5 - 1)
			temp_square = random.randint(35, 79)
			temp_price = random.randint(5000000, 15000000)
			temp_time = random.randint(10, 720)

			temp_ad = [temp_district, temp_building_type, temp_square, temp_price, temp_time]
			ALL_ADS.append(temp_ad)
		#__moderate_ad__(number_of_moderations - 1)

def __find_distance__(A, B):
	ans = 0
	for i in range(len(A) - 1):
		ans += (float(B[i]) - float(A[i])) ** 2
	return sqrt(ans)

def __find_nearest__(TEMP):
	global ALL_ADS;
	all_ads_distance = [];
	for i in range(len(ALL_ADS)):
		all_ads_distance.append(ALL_ADS[i] + [__find_distance__(ALL_ADS[i], TEMP)])

	sorted(all_ads_distance, key=lambda all_ads_distance: all_ads_distance[5])
	print(all_ads_distance[:5])

def __input_ad__():
	global DISTRICT_dict, BUILDINGTYPE;
	temp_district = DISTRICT_dict[input("Input District: ")]
	temp_building_type = BUILDINGTYPE_dict[input("Input BuildingType: ")]
	temp_square = float(input("Input Square: "))
	temp_price = float(input("Input Price: "))

	TEMP = [temp_district, temp_building_type, temp_square, temp_price]
	__find_nearest__(TEMP)

def __main__():
	__start__()
	__moderate_ad__(10000)
	__input_ad__()
__main__()
#print(ALL_ADS)
