'''
	Test MVP 1.0.0 Variant Cell method
'''
import random
from math import *

DISTRICTS = ['Арбат', 'Басманный', 'Замоскворечье', 'Красносельский', 'Мещанский', 'Пресненский', 'Таганский', 'Тверской', 'Хамовники', 'Якиманка', 'Аэропорт', 'Беговой', 'Бескудниковский', 'Войковский', 'Восточное Дегунино', 'Головинский', 'Дмитровский', 'Западное Дегунино', 'Коптево', 'Левобережный', 'Молжаниновский', 'Савеловский', 'Сокол', 'Тимирязевский', 'Ховрино', 'Хорошевский', 'Алексеевский', 'Алтуфьевский', 'Бабушкинский', 'Бибирево', 'Бутырский', 'Лианозово', 'Лосиноостровский', 'Марфино', 'Марьина роща', 'Останкинский', 'Отрадное', 'Ростокино', 'Свиблово', 'Северное Медведково', 'Северный', 'Южное Медведково', 'Ярославский', 'Богородское', 'Вешняки', 'Восточное Измайлово', 'Восточный', 'Гольяново', 'Ивановское', 'Измайлово', 'Косино-Ухтомский', 'Метрогородок', 'Новогиреево', 'Новокосино', 'Перово', 'Преображенское', 'Северное Измайлово', 'Соколиная гора', 'СокольникиВыхино-Жулебино', 'Капотня', 'Кузьминки', 'Лефортово', 'Люблино', 'Марьино', 'Некрасовка', 'Нижегородский', 'Печатники', 'Рязанский', 'Текстильщики', 'Южнопортовый', 'Выхино-Жулебино', 'Бирюлево Восточное', 'Бирюлево Западное', 'Братеево', 'Даниловский', 'Донской', 'Зябликово', 'Москворечье-Сабурово', 'Нагатино-Садовники', 'Нагатинский Затон', 'Нагорный', 'Орехово-Борисово Северное', 'Орехово-Борисово Южное', 'Царицыно', 'Чертаново Северное', 'Чертаново Центральное', 'Чертаново Южное', 'Академический', 'Гагаринский', 'Зюзино', 'Коньково', 'Котловка', 'Ломоносовский', 'Обручевский', 'Северное Бутово', 'Теплый Стан', 'Черемушки', 'Южное Бутово', 'Ясенево', 'Внуково', 'Дорогомилово', 'Крылатское', 'Кунцево', 'Можайский', 'Ново-Переделкино', 'Очаково-Матвеевское', 'Проспект Вернадского', 'Раменки', 'Солнцево', 'Тропарево-Никулино', 'Филевский парк', 'Фили-Давыдково', 'Куркино', 'Митино', 'Покровское-Стрешнево', 'Северное Тушино', 'Строгино', 'Хорошево-Мневники', 'Щукино', 'Южное Тушино', 'Крюково', 'Матушкино', 'Савелки', 'Силино', 'Старое Крюково', 'Внуковское', 'Воскресенское', 'Десеновское', 'Кокошкино', 'Марушкинское', 'Московский', 'Мосрентген', 'Рязановское', 'Сосенское', 'Филимонковское', 'Щербинка']
ALL_ADS = []
CELL_high = 100
CELL_width = 100
CELL_min_num_ads = 10
CELL_max_num_ads = 1000
DISTRICTS_num = len(DISTRICTS)
BASE_info = dict()

class DISTRICT(object):
	def __init__(self, district, metro, price_metr, graph):
		self.district = district
		self.metro = metro
		self.price_metr = price_metr
		self.graph = graph

class CELL(object):
    def __init__(self, price, square, district, cell):
        self.price = price
        self.square = square
        self.district = district
        self.cell = cell

class ADD(object):
    def __init__(self, price, square, district, cell):
        self.price = price
        self.square = square
        self.district = district
        self.cell = cell

def creating_base():
	global BASE_info
	temp = open("districts_information.txt", "r").read().split("\n")
	for i in range(len(temp)):
		string = temp[i].split("\t")[2:]
		district, metro, price_metr, graph = string[1], string[2], int(string[3].replace(" ", "")), float(string[4].replace(",", ".").replace("%", ""))
		BASE_info[DISTRICT(district, metro, price_metr, graph).district] = DISTRICT(district, metro, price_metr, graph)

def generating_price(square, district):
	global BASE_info
	return BASE_info[district].price_metr * square

def generating_main(cell):
	global DISTRICTS_num
	square = random.randint(20, 1000)
	district = random.randint(1, DISTRICTS_num)
	price = generating_price
	add = ADD(price, square, district, cell)
	return add

def forming_ads():
	global ALL_ADS, CELL_high, CELL_width;
	for cell in range(CELL_high * CELL_width):
		ALL_ADS.append(generating_main(cell))
	return 0;

if __name__ == "__main__":
	creating_base()
	forming_ads()