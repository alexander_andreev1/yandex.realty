'''
	Test MVP 2.0.0 Area method
	Only three params: District, BuildingType, Square, Price, Time (Days)
'''
import random
from math import *

DISTRICT_list = ["Гольяново", "Метрогородок", "Северное Измайлово"]
DISTRICT_dict = {}

PRICE_METR = [175479, 183617, 173226]
BUILDINGTYPE_list = ["Кирпич", "Монолит", "Панель", "Кирпич-монолит", "Блок"]
BUILDINGTYPE_dict = {}

# Коэффициенты важности параметров
K_IMPORTANCE = [1, 1, 0.6, 0.5, 1]

CELL_graph = []; # i-й квадрат связан с всеми квадратами списка

ALL_ADS = dict()
CELL_num = 100;
MIN_PRICE = 5000000;
MAX_PRICE = 50000000;
MIN_TIME = 30;
MAX_TIME = 1000;
MIN_SQUARE = 35;
MAX_SQUARE = 79;

# Бинарный поиск позиции в temp_list, когда значение cell в temp_add будет вставлено в temp_list в порядке возрастания
def __bin_find_index__(temp_list, temp_ad):
	value = temp_ad[5]
	mid = len(temp_list) // 2
	low = 0
	high = len(temp_list) - 1
	 
	while temp_list[mid][5] != value and low <= high:
	    if value > temp_list[mid][5]:
	        low = mid + 1
	    else:
	        high = mid - 1
	    mid = (low + high) // 2
	 
	if low > high:
	    return(len(temp_list))
	else:
	    return(mid)

def __start__():
	global DISTRICT_list, DISTRICT_dict, BUILDINGTYPE_list, BUILDINGTYPE_dict;

	for key in range(len(DISTRICT_list)):
		DISTRICT_dict[str(DISTRICT_list[key])] = str(key + 1)
	for key in range(len(BUILDINGTYPE_list)):
		BUILDINGTYPE_dict[str(BUILDINGTYPE_list[key])] = str(key + 1)
	print(DISTRICT_dict)

# Модерирование объявлений в количестве number_of_moderations
def __moderate_ad__(number_of_moderations):
	global PRICE_METR, All, CELL_num, MIN_SQUARE, MAX_SQUARE, MIN_TIME, MAX_TIME, MIN_PRICE, MAX_PRICE, BUILDINGTYPE_list, DISTRICT_list;
	for i in range(number_of_moderations):
		if number_of_moderations > 0:
			temp_district = random.randint(0, len(DISTRICT_list) - 1)
			temp_building_type = random.randint(1, len(BUILDINGTYPE_list) - 1)
			temp_square = random.randint(MIN_SQUARE, MAX_SQUARE)
			temp_price = random.randint(MIN_PRICE, MAX_PRICE)
			temp_time = random.randint(MIN_TIME, MAX_TIME)
			temp_cell = random.randint(1, CELL_num)

			temp_ad = [temp_district - 1, temp_building_type, temp_square, PRICE_METR[temp_district - 1] * temp_square, temp_time, temp_cell]
			key_exists = DISTRICT_list[temp_district] in ALL_ADS;
			if key_exists == False:
				ALL_ADS[DISTRICT_list[temp_district]] = [temp_ad];
			else:
				temp_list = ALL_ADS[DISTRICT_list[temp_district]] 
				temp_list.insert(__bin_find_index__(temp_list, temp_ad), temp_ad)
				ALL_ADS[DISTRICT_list[temp_district]] = temp_list;

def __output__(temp_list, TEMP):
	print()
	global DISTRICT_list, BUILDINGTYPE_list
	for i in range(len(temp_list)):
		print("Top №" + str(i + 1))
		print("District: " + DISTRICT_list[temp_list[i][0]]);
		print("BuildingType: " + BUILDINGTYPE_list[temp_list[i][1]]);
		print("Square: " + str(temp_list[i][2]) + "m²" + " (" + str(temp_list[i][2] - TEMP[2]) + ")");
		print("Price: " + str(temp_list[i][3]) + "rub." + " (" + str(temp_list[i][3] - TEMP[3]) + ")");
		print("Days before sale: " + str(temp_list[i][4]) + "days");
		print("Cell: " + str(temp_list[i][5]) + " (" + str(temp_list[i][5] - TEMP[4]) + ")");
		print("Tech. Distance: " + str(temp_list[i][6]));
		print();

def __find_distance__(A, B):
	global K_IMPORTANCE;
	ans = 0
	for i in range(len(A) - 1):
		ans += K_IMPORTANCE[i] * (float(B[i]) - float(A[i])) ** 2
	return sqrt(ans)

def __find_nearest__(TEMP):
	global ALL_ADS, DISTRICT_list;

	all_ads_distance = ALL_ADS[DISTRICT_list[int(TEMP[0])]];
	for i in range(len(all_ads_distance)):
		all_ads_distance[i] += [__find_distance__(all_ads_distance[i], TEMP)]

	sorted(all_ads_distance, key=lambda all_ads_distance: all_ads_distance[5])
	__output__(all_ads_distance[:5], TEMP)

def __input_ad__():
	global DISTRICT_dict, BUILDINGTYPE;
	temp_district = DISTRICT_dict[input("Input District: ")]
	temp_building_type = BUILDINGTYPE_dict[input("Input BuildingType: ")]
	temp_square = float(input("Input Square: "))
	temp_price = float(input("Input Price: "))
	temp_cell = int(input("Input Cell Number: "))

	TEMP = [temp_district, temp_building_type, temp_square, temp_price, temp_cell]
	__find_nearest__(TEMP)

def __main__():
	__start__()
	__moderate_ad__(200)
	__input_ad__()
__main__()
#print(ALL_ADS)